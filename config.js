// "gulp" for development
// "gulp prod" for production CSS

const config = {
    projectName: "HubSpot Project",
    download: "https://www.digitalbasel.org/en/",
    css: "digitalbasel-styles", // do not need to add extension here. File in downloaded page (index.html) will be searched with regex to find this file and replace it with .css extension.
    js: "digitalbasel-scripts",
    puppetter: false, //set it to true to enable download puppetter plugin - it will make CTA display properly, but HTML download time will be longer. Relunch gulp after changing it.
    prodBuild: "template" //set to "theme" to make "gulp prod" generate hubl variables for themes. Default: "template"
}

// export config, so it can be accessable with "require('./config.js)" or with eval() method
module.exports = config;